package io.github.xxyy.tupletest;

import jline.console.ConsoleReader;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

/**
 * @author <a href="http://xxyy.github.io/">xxyy</a>
 * @since 5.10.15
 */
public class TupleTest {
    public static void main(String[] args) throws IOException {
        ConsoleReader reader = new ConsoleReader();
        String strLimit = reader.readLine("Enter limit: ");

        int limit = Integer.parseInt(strLimit);
        Set<Triple> validTriples = new HashSet<>();
        int prevStatusLength = 0;

        for (int a = 1; a <= limit; a++) {
            for (int b = 1; b <= limit; b++) {
                for (int c = 1; c <= limit; c++) {
                    if (Triple.isValid(a, b, c)) {
                        validTriples.add(new Triple(a, b, c));
                    }
                }
            }
            renderProgress(reader, a, limit, 10, validTriples);
        }

        reader.clearScreen();

        System.out.println("\n\n----- results ----\n");
        System.out.println("checked every integer up to " + limit + ".");
        System.out.println("matching tuples:");

        validTriples.stream()
                .map(Object::toString)
                .forEach(System.out::println);
    }

    private static void renderProgress(ConsoleReader reader, int a, int limit, int step, Set<Triple> validTriples) throws IOException {
        if ((a % step) != 0) {
            return;
        }
        reader.clearScreen();
        System.out.println("------ TupleTest ------");

        StringBuilder dots = new StringBuilder();
        for (int i = 0; i < (a / step); i++) {
            dots.append(".");
        }

        System.out.println(dots.toString() + " ( " + a + " / " + limit + " )");

        System.out.println("\n\n----- currrent results ----\n");
        validTriples.stream()
                .map(Object::toString)
                .forEach(System.out::println);

    }
}
