package io.github.xxyy.tupletest;

/**
 * @author <a href="http://xxyy.github.io/">xxyy</a>
 * @since 5.10.15
 */
public class Triple {
    public int a;
    public int b;
    public int c;

    public Triple(int a, int b, int c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public static boolean isValid(int a, int b, int c) {
        return (a != b && b != c && a != c) &&
                gcd(a, b) == 1 &&
                gcd(b, c) == 1 &&
                gcd(a, c) == 1 &&
                ((b + c) % a) == 0 &&
                ((a + b) % c) == 0 &&
                ((a + c) % b) == 0;
    }

    @Override
    public String toString() {
        return "<" + a + ", " + b + ", " + c + ">";
    }

    public static int gcd(int a, int b) { //greatest common divisor, ggT
        if (b == 0) return a;
        return gcd(b, a % b);
    }
}
